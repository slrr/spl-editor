﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using SlrrLib.View;

namespace SplEditor
{
  public partial class MainWindow : Window
  {
    private string splFileName = "";
    private float tickForMove = 0.01f;
    private SlrrLib.Geom.MapRpkModelFactory currentFactory;
    private SlrrLib.Geom.SplGeometry splGeom = null;
    private SlrrLib.Geom.NamedModel splPivotName = new SlrrLib.Geom.NamedModel
    {
      Translate = new Vector3D(),
      Name = "Spl - Line",
      ModelGeom = new GeometryModel3D()
    };

    public MainWindow()
    {
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        ctrlOrbitingViewport.LastRpkDirectory = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      ctrlOrbitingViewport.MarkerMoved += ctrlOrbitingViewport_MarkerMoved;
      splGeom = new SlrrLib.Geom.SplGeometry(new SlrrLib.Model.Spl());
    }

    private void loadSceneRpk()
    {
      currentFactory = ctrlOrbitingViewport.LoadSceneRpk();
      if(currentFactory == null)
        return;
      Title = ctrlOrbitingViewport.LastRpkOpened;
      splPivotName = new SlrrLib.Geom.NamedModel
      {
        Translate = new Vector3D(),
        Name = "Spl - Line",
        ModelGeom = new GeometryModel3D()
      };
      ctrlOrbitingViewport.AddModelToScene(splPivotName);
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
    private void copyArgsInCircularSpl()
    {
      if (splGeom != null && splGeom.Spl != null && splGeom.Spl.IsCircular())
      {
        var first = splGeom.Spl.SplLines[0];
        var last = splGeom.Spl.SplLines[splGeom.Spl.SplLines.Count - 1];
        last.additionalAttributes = new SlrrLib.Model.SplAttributes(first.additionalAttributes);
      }
    }
    private void refreshSplGeom(bool visualiseNormals = false)
    {
      if (splGeom == null)
        return;
      copyArgsInCircularSpl();
      splPivotName.ModelGeom.Geometry = splGeom.MeshFromModelData(10, visualiseNormals);
      ctrlOrbitingViewport.SetModelToColor(splPivotName, Colors.SkyBlue, 0.8, 0.8, Colors.SkyBlue);
    }
    private float getMouseMoveTick(MouseWheelEventArgs e)
    {
      return Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 100.0f : 10.0f);
    }
    private SlrrLib.Model.SplLine getClosestMarkerPos()
    {
      if (splGeom == null)
        return null;
      var toad = new SlrrLib.Model.SplLine();
      toad.position.x = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.X;
      toad.position.y = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Y;
      toad.position.z = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Z;
      float curMin = float.MaxValue;
      SlrrLib.Model.SplLine curMinPos = null;
      foreach (var ln in splGeom.Spl.SplLines)
      {
        float posMin = (toad.position - ln.position).Length();
        if (posMin < curMin)
        {
          curMinPos = ln;
          curMin = posMin;
        }
      }
      if (curMinPos != null)
      {
        return curMinPos;
      }
      return null;
    }
    private void handleMarkerTextboxChange()
    {
      var spl = getClosestMarkerPos();
      if (spl != null)
      {
        float tmp = 0;
        int tmp_i = 0;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerX.Text, out tmp))
          spl.position.x = tmp;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerY.Text, out tmp))
          spl.position.y = tmp;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerZ.Text, out tmp))
          spl.position.z = tmp;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerWidth.Text, out tmp))
          spl.additionalAttributes.splineWidth = tmp;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerAccel.Text, out tmp))
          spl.additionalAttributes.speedRatio = tmp;
        if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerUnkwn.Text, out tmp_i))
          spl.additionalAttributes.unknown = tmp_i;
        refreshSplGeom();
      }
    }

    private void ctrlOrbitingViewport_MarkerMoved(object sender, EventArgs e)
    {
      var spl = getClosestMarkerPos();
      if (spl != null)
      {
        ctrlTextBoxMarkerX.Text = UIUtil.FloatToString(spl.position.x);
        ctrlTextBoxMarkerY.Text = UIUtil.FloatToString(spl.position.y);
        ctrlTextBoxMarkerZ.Text = UIUtil.FloatToString(spl.position.z);
        ctrlTextBoxMarkerWidth.Text = UIUtil.FloatToString(spl.additionalAttributes.splineWidth);
        ctrlTextBoxMarkerAccel.Text = UIUtil.FloatToString(spl.additionalAttributes.speedRatio);
        ctrlTextBoxMarkerUnkwn.Text = spl.additionalAttributes.unknown.ToString();
      }
    }
    private void ctrlButtonAddSpl_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = "",
        DefaultExt = ".spl",
        Filter = "spls|*.spl"
      };

      var result = dlg.ShowDialog();
      if (result == true)
      {
        splFileName = dlg.FileName;
        splGeom = new SlrrLib.Geom.SplGeometry(new SlrrLib.Model.Spl(splFileName));
        refreshSplGeom(true);
      }
    }
    private void ctrlButtonClearSpl_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom != null && splGeom.Spl != null)
      {
        splGeom.Spl.SplLines.Remove(getClosestMarkerPos());
        refreshSplGeom();
      }
    }
    private void ctrlButtonAddSplNode_Click(object sender, RoutedEventArgs e)
    {
      var toad = new SlrrLib.Model.SplLine();
      toad.position.x = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.X;
      toad.position.y = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Y;
      toad.position.z = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Z;
      if (splGeom.Spl.SplLines.Count > 2)
        toad.additionalAttributes = new SlrrLib.Model.SplAttributes(splGeom.Spl.SplLines[1].additionalAttributes);
      else
        toad.additionalAttributes = new SlrrLib.Model.SplAttributes(2.0f, 100.0f, 0);
      if (splGeom.Spl.SplLines.Count == 0)
      {
        splGeom.Spl.SplLines.Insert(0, toad);
        splGeom.Spl.SplLines.Insert(0, toad);
      }
      else
        splGeom.Spl.SplLines.Insert(1, toad);
      splGeom.Spl.MakeCircularIfNeeded();
      splGeom.Spl.FillNormalsFromPositiondata();
      refreshSplGeom();
    }
    private void ctrlButtonSaveSpl_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom == null)
        return;
      if (splGeom.Spl == null)
        return;
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = splFileName,
        DefaultExt = ".spl",
        Filter = "spls|*.spl"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        splFileName = dlg.FileName;
        splGeom.Spl.MakeCircularIfNeeded();
        splGeom.Spl.Save(dlg.FileName);
        StringBuilder sb = new StringBuilder();
        List<SlrrLib.Model.SplLine> startToFinish = new List<SlrrLib.Model.SplLine>();
        bool wasStart = false;
        bool wasEnd = false;
        int spl_i = splGeom.Spl.SplLines.Count - 1;
        SlrrLib.Model.SplNode lastWritten = null;
        if (splGeom.Start != null && splGeom.Finish != null)
        {
          while (!wasEnd)
          {
            var splLine = splGeom.Spl.SplLines[spl_i];
            if (splLine == splGeom.Start)
            {
              wasStart = true;
              sb.AppendLine("oneRound.StartPosition = new Vector3(" + splLine.position.ToString().Replace(" ", ",") + ");");
              lastWritten = splLine.position;
            }
            if (wasStart)
            {
              if (splLine == splGeom.Finish)
              {
                sb.AppendLine("oneRound.FinishPosition = new Vector3(" + splLine.position.ToString().Replace(" ", ",") + ");");
                wasEnd = true;
                break;
              }
              if (lastWritten == null || (splLine.position - lastWritten).Length() > 10.0f)
              {
                sb.AppendLine("oneRound.checkPoints.addElement(new Vector3(" + splLine.position.ToString().Replace(" ", ",") + "));");
                lastWritten = splLine.position;
              }
            }
            spl_i--;
            if (spl_i < 0)
              spl_i = splGeom.Spl.SplLines.Count - 1;
          }
          System.IO.File.WriteAllText(dlg.FileName + ".txt", sb.ToString());
        }
        refreshSplGeom(true);
      }
    }
    private void ctrlButtonSnap_Click(object sender, RoutedEventArgs e)
    {
      var toad = new SlrrLib.Model.SplLine();
      toad.position.x = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.X;
      toad.position.y = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Y;
      toad.position.z = (float)ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace.Z;
      float curMin = float.MaxValue;
      SlrrLib.Model.SplLine curMinPos = null;
      foreach (var ln in splGeom.Spl.SplLines)
      {
        float posMin = (toad.position - ln.position).Length();
        if (posMin < curMin)
        {
          curMinPos = ln;
          curMin = posMin;
        }
      }
      if (curMinPos != null)
      {
        if (curMinPos == splGeom.Spl.SplLines.First() ||
            curMinPos == splGeom.Spl.SplLines.Last())
        {
          if (splGeom.Spl.IsCircular())
          {
            splGeom.Spl.SplLines.First().position.CopyFrom(toad.position);
            splGeom.Spl.SplLines.Last().position.CopyFrom(toad.position);
          }
        }
        curMinPos.position.CopyFrom(toad.position);
        splGeom.Spl.FillNormalsFromPositiondata();
        refreshSplGeom();
      }
    }
    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk();
    }
    private void ctrlButtonAddToScene_Click(object sender, RoutedEventArgs e)
    {
      ctrlOrbitingViewport.LoadSecondaryRpk();
    }
    private void ctrlButtonRandomizeSpl_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom == null)
        return;
      if (splGeom.Spl == null)
        return;
      Random rnd = new Random();
      if (splGeom.Spl.IsCircular())
      {
        try
        {
          foreach (var ln in splGeom.Spl.SplLines.Skip(2).Take(splGeom.Spl.SplLines.Count - 4))
          {
            ln.position.x += ((float)rnd.NextDouble() - 0.5f) * 1.0f;
            ln.position.z += ((float)rnd.NextDouble() - 0.5f) * 1.0f;
          }
        }
        catch (Exception)
        {
          MessageBox.Show("Can't randomize too few segments");
        }
      }
      else
      {
        foreach (var ln in splGeom.Spl.SplLines)
        {
          ln.position.x += ((float)rnd.NextDouble() - 0.5f) * 1.0f;
          ln.position.z += ((float)rnd.NextDouble() - 0.5f) * 1.0f;
        }
      }
      refreshSplGeom();
    }
    private void ctrlTextBoxMarker_KeyUp(object sender, KeyEventArgs e)
    {
      handleMarkerTextboxChange();
    }
    private void ctrlTextBoxMarkerX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerX.Text, out float tmp))
        ctrlTextBoxMarkerX.Text = UIUtil.FloatToString(tmp + getMouseMoveTick(e));
    }
    private void ctrlTextBoxMarkerY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerY.Text, out float tmp))
        ctrlTextBoxMarkerY.Text = UIUtil.FloatToString(tmp + getMouseMoveTick(e));
    }
    private void ctrlTextBoxMarkerZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerZ.Text, out float tmp))
        ctrlTextBoxMarkerZ.Text = UIUtil.FloatToString(tmp + getMouseMoveTick(e));
    }
    private void ctrlTextBoxMarkerUnkwn_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerUnkwn.Text, out float tmp))
        ctrlTextBoxMarkerUnkwn.Text = ((int)(tmp + getMouseMoveTick(e))).ToString();
    }
    private void ctrlTextBoxMarkerAccel_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerAccel.Text, out float tmp))
        ctrlTextBoxMarkerAccel.Text = UIUtil.FloatToString(tmp + getMouseMoveTick(e));
    }
    private void ctrlTextBoxMarkerWidth_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerWidth.Text, out float tmp))
        ctrlTextBoxMarkerWidth.Text = UIUtil.FloatToString(tmp + getMouseMoveTick(e));
    }
    private void ctrlButtonGenAllNormals_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom == null)
        return;
      if (splGeom.Spl == null)
        return;
      splGeom.Spl.MakeCircularIfNeeded();
      splGeom.Spl.FillNormalsFromPositiondata();
    }
    private void ctrlButtonMarkStart_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom != null && splGeom.Spl != null)
      {
        splGeom.Start = getClosestMarkerPos();
        refreshSplGeom();
      }
    }
    private void ctrlButtonMarkFinish_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom != null && splGeom.Spl != null)
      {
        splGeom.Finish = getClosestMarkerPos();
        refreshSplGeom();
      }
    }
    private void ctrlButtonDuplicate_Click(object sender, RoutedEventArgs e)
    {
      if (splGeom != null && splGeom.Spl != null)
      {
        var closest = getClosestMarkerPos();
        var toad = new SlrrLib.Model.SplLine();
        toad.position.x = closest.position.x + 5.0f;
        toad.position.y = closest.position.y;
        toad.position.z = closest.position.z;
        toad.additionalAttributes.speedRatio = closest.additionalAttributes.speedRatio;
        toad.additionalAttributes.splineWidth = closest.additionalAttributes.splineWidth;
        toad.additionalAttributes.unknown = closest.additionalAttributes.unknown;
        int ind = splGeom.Spl.SplLines.IndexOf(closest);
        splGeom.Spl.SplLines.Insert(ind, toad);
        splGeom.Spl.MakeCircularIfNeeded();
        splGeom.Spl.FillNormalsFromPositiondata();
        refreshSplGeom();
      }
    }
  }
}
